(use-modules (gnu)
             (guix)
             (guix download))
(use-service-modules networking ssh shepherd)
(use-package-modules certs
                     screen
                     ssh
                     tmux
                     vim
                     wget)

(operating-system
  (host-name "msg")
  (timezone "America/Denver")
  (locale "en_US.utf8")

  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets '("/boot/efi"))
                (terminal-outputs '(console))))

  (file-systems (cons* (file-system
			(mount-point "/boot/efi")
			(device "/dev/vda1")
			(type "vfat"))
		       (file-system
                        (mount-point "/")
                        (device "/dev/vda2")
                        (type "ext4")) 
		       %base-file-systems))

  (users (cons (user-account
                 (name "admin")
                 (group "users")
                 (supplementary-groups '("wheel" "audio" "video")))
               %base-user-accounts))

  (sudoers-file (plain-file "sudoers"
                            "root ALL=(ALL) ALL\n%wheel ALL=NOPASSWD: ALL\n"))

  (packages (append (list tmux nss-certs vim wget) %base-packages))

  (services
    (append
      (list (service openssh-service-type)
            (service network-manager-service-type)
            (service wpa-supplicant-service-type)
        (simple-service
                   'mount-macos-overlayfs
                   shepherd-root-service-type
                   (list
                    (shepherd-service
                     (provision '(9pfs-macos))
		     (requirement '(user-processes))
                     (documentation "Mount macos 9pfs")
		     (one-shot? #t)
                     (start (let ((util-linux (@ (gnu packages linux) util-linux)))
                              #~(lambda _
                                  (system*
                                   #$(file-append util-linux "/bin/mount")
                                   "-t" "9p"
                                   "-o" "trans=virtio,cache=mmap"
                                   "macos" "/home/admin" "-oversion=9p2000.L")
                                  #f)))
                               )))
           (simple-service
                   'permissions-macos-overlayfs
                   shepherd-root-service-type
                   (list
                    (shepherd-service
                     (provision '(permissions-macos))
                     (documentation "Mount macos 9pfs")
		     (one-shot? #t)
                     (start (let ((util-linux (@ (gnu packages linux) util-linux)))
                              #~(lambda _
                                  (system*
                                   #$(file-append util-linux "chown")
                                   "-R" "admin"
                                   "/home/admin")
                                  #f)))
                               )))

            (service ntp-service-type))
      %base-services))
  (name-service-switch %mdns-host-lookup-nss))
