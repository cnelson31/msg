# ********** This repository has been moved to [[https://forge.superkamiguru.org/MSG/msg-cli]] ************

# MSG (Mac Subsystem for Guix)
## Description
MSG is an attempt to create a native-like experience for GUIX on MacOS, using methods inspired by others like Podman/Docker/Lima.

# !!!!Currently only compatible with M1/M2 Macs
## README (WIP)

### Prerequisites: 
- qemu
- wget
- git
- xquartz (optional for GUI applications)

### Installation
1. clone the project using `git clone git@gitlab.com:cnelson31/msg.git ~/.guix`
2. copy Guix.app from ~/.guix to your Applications folder
3. Run the guix application (this will take a minute to download the most recent image)
4. add these lines to your shell rc file (bashrc/zshrc) to access the guix environment from MacOS :
   ```
   alias guix="ssh -i $HOME/.guix/ssh-cert/msg_rsa admin@127.0.0.1 -p 9001 'guix'" 
   alias guix-env='f(){ ssh -i $HOME/.guix/ssh-cert/msg_rsa admin@127.0.0.1 -p 9001 /home/admin/.guix-profile/bin/$@;  unset -f f; }; f'
   alias guix-app='f(){ ssh -X -i $HOME/.guix/ssh-cert/msg_rsa admin@127.0.0.1 -p 9001 /home/admin/.guix-profile/bin/$@;  unset -f f; }; f'
   alias guix-shell="ssh -i $HOME/.guix/ssh-cert/msg_rsa admin@127.0.0.1 -p 9001"
   ```
5. Make sure to restart your terminal or source your shellrc file after editing it (e.g `source ~/.bashrc`)

### Available Commands (after adding bashrc/zshrc options)
- guix : A straight passthrough to the guix application
- guix-shell: Connect to the vm over ssh
- guix-env : Can be used to pass cli commands interactively
- guix-app : Used to passthrough X11 applications from the vm to macos using xquartz

### Notes:
- Currently `guix-env sudo guix system reconfigure /etc/config.scm` is not supported. You must use `guix-shell` to ssh into the environment and then run `sudo guix system reconfigure /etc/config.scm`
- The host home directory is mounted as /mnt/macos in the virtual environment
- The default user/credentials are admin/admin
- For xquartz to be able to display X11 apps forwarded from the vm, you must open xquartz and go to Top Menu->Xquartz->Settings->Security->Enable 'Allow connections from network clients'
- If xquartz is enabled, you can use the .app files under ~/.guix/apps to launch guix programs from macos

### Discord:
Feel free to join the discord server and ask questions/share your experiences!
https://discord.gg/TqMDT6Gf6W

### Interested in supporting the project?
Ko-fi : https://ko-fi.com/superkamiguru
